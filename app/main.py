from concurrent.futures import ThreadPoolExecutor
from grpc import server, Server
from time import sleep
from sys import stdout
from dot import Renderer

from gp2d_pb2 import *
from gp2d_pb2_grpc import *


class Service(GP2DServicer):

    def __init__(self, renderer):
        self._renderer = renderer

    def Render(self, request, context):
        if request.graph is None or request.graph == '':
            return DotResponse(error='An input graph must be provided.')

        graph, error = self._render(request.graph)

        if graph is None or graph == '':
            return DotResponse(error=error)

        return DotResponse(graph=graph)

    def _render(self, graph):
        try:
            return self._renderer.render(graph)
        except BaseException as e:
            print(e)
            stdout.flush()
            return None, 'An unexpected error occured.'


class Factory(object):

    @staticmethod
    def make(service = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        s = server(ThreadPoolExecutor(max_workers=16), options=GRPC_CHANNEL_OPTIONS)
        add_GP2DServicer_to_server(service or Factory._make_service(), s)
        s.add_insecure_port('[::]:9111')

        return s

    @staticmethod
    def _make_service(gp2d_path = None):
        return Service(
            Renderer(gp2d_path or 'gp2d')
        )


def main():
    s = Factory.make()
    s.start()

    try:
        while True:
            sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        s.stop(0)


if __name__ == '__main__':
    main()
