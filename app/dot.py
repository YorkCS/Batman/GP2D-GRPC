from subprocess import run, CalledProcessError, PIPE, STDOUT
from uuid import uuid4
from os import environ


class Renderer(object):

    def __init__(self, binary):
        self._binary = binary

    def render(self, graph):
        path = '/tmp/' + str(uuid4())
        with open(path, 'w') as f:
            f.write(graph)

        try:
            return run([self._binary, path], env=environ.copy(), check=True, stdout=PIPE, stderr=STDOUT).stdout, None
        except CalledProcessError as e:
            return None, self._sanitize(e.output or b'Failed to render!', path)

    def _sanitize(self, data, path):
        return data.replace(bytes(path, 'utf-8'), b'').replace(b'  ', b' ')
