FROM python:3.9-slim-buster
RUN pip install grpcio-tools
COPY proto /proto
WORKDIR /proto
RUN mkdir build
RUN python -m grpc_tools.protoc -I. --python_out=build --grpc_python_out=build *.proto

FROM registry.gitlab.com/yorkcs/batman/gp2d:latest
RUN curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py" && python3 get-pip.py && rm get-pip.py
RUN pip3 install grpcio protobuf
COPY app /app
COPY --from=0 /proto/build /app
WORKDIR /app
EXPOSE 9111
ENTRYPOINT ["python3", "main.py"]
