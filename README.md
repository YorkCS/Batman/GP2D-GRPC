# GP2D GRPC

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

Usage of the GP2D GRPC Docker image is easy. Simply start the daemon and off we go!

```bash
$ docker run -d -p 9111:9111 registry.gitlab.com/yorkcs/batman/gp2d-grpc
```

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2d-grpc:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2d-grpc
```
